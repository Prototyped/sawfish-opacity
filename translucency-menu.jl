;; translucency-menu.jl: Composite-based translucency effects for
;;                       all windows
;; $Id$

;; Copyright (C) 2005 Amit Gurdasani <amitg@alumni.cmu.edu>

;; This file is not part of sawmill.
;;
;;  This software is provided 'as-is', without any express or implied
;;  warranty.  In no event will the authors be held liable for any damages
;;  arising from the use of this software.
;;
;;  Permission is granted to anyone to use this software for any purpose,
;;  including commercial applications, and to alter it and redistribute it
;;  freely, subject to the following restrictions:
;;
;;  1. The origin of this software must not be misrepresented; you must not
;;     claim that you wrote the original software. If you use this software
;;     in a product, an acknowledgment in the product documentation would be
;;     appreciated but is not required.
;;  2. Altered source versions must be plainly marked as such, and must not be
;;     misrepresented as being the original software.
;;  3. This notice may not be removed or altered from any source distribution.
;;
;; Usage: (require 'translucency-menu)
;;        Ensure that a composition manager (such as xcompmgr) is running.
;;	  For the window history feature, translucency-matcher.jl is required.

;; If window-history is loaded, add a snapshotter and setter
;; so that opacity is remembered across sessions.

(if (not (or (eq (find-symbol "translucency-matcher") nil)
	     (eq (find-symbol "sawfish.wm.ext.window-history") nil)))
   (let ()
      (put 'opacity 'window-history-snapshotter
	 (lambda (w)
	    (let ((alpha-value
		     (caddr (get-x-property (window-frame-id w)
			       '_NET_WM_WINDOW_OPACITY))))
	       (cond ((eq alpha-value nil)
			(setq alpha-value #xffffffff))
		  (t (setq alpha-value (aref alpha-value 0))))
	       alpha-value)))
      
      ;; Not reparented yet, so we set its own opacity property and wait for
      ;; the visibility-notify-hook to invoke inherit-opacity in
      ;; translucency-matcher.
      
      (put 'opacity 'window-history-setter
	 (lambda (w alpha)
	    (set-x-property w '_NET_WM_WINDOW_OPACITY
	       (make-vector 1 alpha) 'CARDINAL 32)))
      
      (setq window-history-states (cons 'opacity window-history-states))))

;; Add menu entries to control translucency.

(letrec
   ((gen-alpha-list
      (lambda (beg end iter res)
	 (cond
	    ((eq beg (+ end iter)) res)
	    (t (gen-alpha-list beg (- end iter) iter (cons end res))))))
    (opacity-menu nil)
    (set-opacity (lambda (w alpha)
      (setq alpha (* alpha 42949672.95))
      (set-x-property (window-frame-id w) '_NET_WM_WINDOW_OPACITY
	 (make-vector 1 alpha) 'CARDINAL 32)
      (set-x-property w '_NET_WM_WINDOW_OPACITY
	 (make-vector 1 alpha) 'CARDINAL 32)
      (if (not (or (eq (find-symbol "translucency-matcher") nil)
	           (eq (find-symbol "sawfish.wm.ext.window-history") nil)))
	 (window-history-save-attributes w)))))

   (mapc
      (lambda (i)
	 (let*
	    ((menu-text (format nil "Set _%d%% opacity" i))
	     (label (format nil "set-opacity-%d" i))
	     (closure-symbol (intern label)))
	    
	    (define-command closure-symbol 
	       (lambda (w) (set-opacity w i)) #:spec "%W")
	    (setq opacity-menu
	       (append opacity-menu (list 
				    (list (_ menu-text) closure-symbol))))))
      (gen-alpha-list 10 100 10 nil))
   
   (setq window-ops-menu
      `(,@window-ops-menu ("Opacity" ,@opacity-menu))))

(provide 'translucency-menu)
