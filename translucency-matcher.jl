
;; translucency-matcher.jl: Composite-based translucency effects for
;;                          matched windows
;; $Id$

;; Copyright (C) 2005 Amit Gurdasani <amitg@alumni.cmu.edu>

;; This file is not part of sawmill.
;;
;;  This software is provided 'as-is', without any express or implied
;;  warranty.  In no event will the authors be held liable for any damages
;;  arising from the use of this software.
;;
;;  Permission is granted to anyone to use this software for any purpose,
;;  including commercial applications, and to alter it and redistribute it
;;  freely, subject to the following restrictions:
;;
;;  1. The origin of this software must not be misrepresented; you must not
;;     claim that you wrote the original software. If you use this software
;;     in a product, an acknowledgment in the product documentation would be
;;     appreciated but is not required.
;;  2. Altered source versions must be plainly marked as such, and must not be
;;     misrepresented as being the original software.
;;  3. This notice may not be removed or altered from any source distribution.
;;
;; Usage: (require 'translucency-matcher)
;;        Ensure that a composition manager (such as xcompmgr) is running.

(require 'sawfish.wm.ext.match-window)

(define-match-window-property 'opacity 'appearance '(number 1 100))

(define-match-window-setter 'opacity
   (lambda (w prop value)
      (set-x-property w '_NET_WM_WINDOW_OPACITY
	 (make-vector 1 (* value 42949672.95)) 'CARDINAL 32)))
   
(define (inherit-opacity w)
   (define win-opacity (get-x-property w '_NET_WM_WINDOW_OPACITY))
   (if
      (not (eq win-opacity nil))
      (set-x-property (window-frame-id w) '_NET_WM_WINDOW_OPACITY
	 (caddr win-opacity) 'CARDINAL 32)))
   
(add-hook 'visibility-notify-hook inherit-opacity t)

(define (translate-window-type vec i)
   (define atom (aref vec i))
   (cond
      ((eq atom '_NET_WM_WINDOW_TYPE_DESKTOP) "Desktop")
      ((eq atom '_NET_WM_WINDOW_TYPE_DIALOG)  "Dialog")
      ((eq atom '_NET_WM_WINDOW_TYPE_DOCK)    "Dock")
      ((eq atom '_NET_WM_WINDOW_TYPE_TOOLBAR) "Toolbar")
      ((eq atom '_NET_WM_WINDOW_TYPE_MENU)    "Menu")
      ((eq atom '_NET_WM_WINDOW_TYPE_UTILITY) "Utility")
      ((eq atom '_NET_WM_WINDOW_TYPE_SPLASH)  "Splashscreen")
      (t "Normal")))
   
(define-match-window-formatter '_NET_WM_WINDOW_TYPE
   (lambda (vec)
      (let ((i 0) parts)
	 (while (< i (length vec))
	    (when parts
	       (setq parts (cons ?  parts)))
	    (setq parts (cons (translate-window-type vec i) parts))
	    (setq i (1+ i)))
	 (apply concat (nreverse parts)))))

(setq match-window-x-properties 
   (cons '(_NET_WM_WINDOW_TYPE . "Window type") match-window-x-properties))

(provide 'translucency-matcher)
