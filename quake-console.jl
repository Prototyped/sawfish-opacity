;; quake-console.jl: Keyboard-activated console that is normally shaded
;; $Id$

;; Copyright (C) 2004, 2005 Amit Gurdasani <amitg@alumni.cmu.edu>

;; This file is not part of sawmill.
;;
;;  This software is provided 'as-is', without any express or implied
;;  warranty.  In no event will the authors be held liable for any damages
;;  arising from the use of this software.
;;
;;  Permission is granted to anyone to use this software for any purpose,
;;  including commercial applications, and to alter it and redistribute it
;;  freely, subject to the following restrictions:
;;
;;  1. The origin of this software must not be misrepresented; you must not
;;     claim that you wrote the original software. If you use this software
;;     in a product, an acknowledgment in the product documentation would be
;;     appreciated but is not required.
;;  2. Altered source versions must be plainly marked as such, and must not be
;;     misrepresented as being the original software.
;;  3. This notice may not be removed or altered from any source distribution.
;;
;; Usage: (require 'quake-console)
;; 	  and then add a binding for "Quake console shade toggle"
;;	  through sawfish-ui
;;	  Finally, run  aterm -tr +sb -name QuakeConsole  via .xsession/.xinitrc

(defvar quake-console-xpos 0
    "X coordinate position of the Quake-style console")
(defvar quake-console-ypos 0
    "Y coordinate position of the Quake-style console")
(defvar quake-console-temp-xpos (+ quake-console-xpos 1)
    "Temporary X position (meant to get it to repaint)")
(defvar quake-console-temp-ypos (+ quake-console-ypos 1)
    "Temporary Y position (meant to get it to repaint)")
(defvar quake-console-class "^QuakeConsole\000XTerm\000$"
    "X window class string to identify Quake-style console")
(defvar quake-console-title "QuakeConsole.*Konsole"
    "X window title string to identify Quake-style console")
(defvar quake-console-xterm nil
   "The single quake console we're monitoring.")
(defvar quake-console-prev-focus nil
   "The window that had input focus when Quake console was activated")

(defun quake-console-shade-toggle ()
   "Toggle display of Quake Console (XTerm/QuakeConsole)"
   (interactive)
   (when (not (eq quake-console-xterm nil))
      (if (window-visible-p quake-console-xterm)
      	 ((lambda ()
	    (shade-window quake-console-xterm)
	    (hide-window quake-console-xterm)
	    (set-input-focus quake-console-prev-focus)
	    (setq quake-console-prev-focus nil)))

         ((lambda ()
     	    (show-window quake-console-xterm)
     	    (unshade-window quake-console-xterm)
	    (setq quake-console-prev-focus (input-focus))
	    (set-input-focus quake-console-xterm)
      	    (move-window-to quake-console-xterm quake-console-temp-xpos quake-console-temp-ypos)
	    (refresh-window quake-console-xterm))))))

(defun quake-console-before-add (window)
   (when (and (eq quake-console-xterm nil)
   	      (or (string-match quake-console-class
	              (caddr (get-x-property window 'WM_CLASS)) nil t)
		  (string-match quake-console-title
		      (caddr (get-x-property window 'WM_NAME)) nil t)))
      (setq quake-console-xterm window)
      (window-put window 'frame-type 'none)
      (window-put window 'depth 16)
      (window-put window 'raise-on-focus t)
      (window-put window 'focus-when-mapped nil)
      (window-put window 'focus-mode 'enter-exit)
      (window-put window 'sticky t)
      (window-put window 'window-list-skip t)
      (window-put window 'task-list-skip t)
      (window-put window 'skip-tasklist t)
      (window-put window 'no-history t)))

(defun quake-console-map-notify (window)
   (when (eq quake-console-xterm window)
      (set-window-type window 'unframed)
      (set-window-depth window 16)
      (gnome-set-skip-tasklist window)
      (move-window-to quake-console-xterm quake-console-temp-xpos quake-console-temp-ypos)
      (sleep-for 1)
      (refresh-window quake-console-xterm)
      (shade-window window)
      (hide-window window)))

(defun quake-console-destroy-notify (window)
   (when (eq window quake-console-xterm)
      (setq quake-console-xterm nil)
      (when (not (eq quake-console-prev-focus nil))
	 (set-input-focus quake-console-prev-focus)
	 (setq quake-console-prev-focus nil))))

(add-hook 'add-window-hook (function quake-console-before-add))
(add-hook 'map-notify-hook (function quake-console-map-notify))
(add-hook 'destroy-notify-hook (function quake-console-destroy-notify))

(provide 'quake-console)
