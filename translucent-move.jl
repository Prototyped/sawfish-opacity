;; translucent-move.jl: Alpha-blend windows to half their opacity on move
;; $Id$

;; Copyright (C) 2005 Amit Gurdasani <amitg@alumni.cmu.edu>
;; Based on Philip Langdale's work
;; http://mail.gnome.org/archives/sawfish-list/2005-February/msg00023.html

;; This file is not part of sawmill.
;;
;;  This software is provided 'as-is', without any express or implied
;;  warranty.  In no event will the authors be held liable for any damages
;;  arising from the use of this software.
;;
;;  Permission is granted to anyone to use this software for any purpose,
;;  including commercial applications, and to alter it and redistribute it
;;  freely, subject to the following restrictions:
;;
;;  1. The origin of this software must not be misrepresented; you must not
;;     claim that you wrote the original software. If you use this software
;;     in a product, an acknowledgment in the product documentation would be
;;     appreciated but is not required.
;;  2. Altered source versions must be plainly marked as such, and must not be
;;     misrepresented as being the original software.
;;  3. This notice may not be removed or altered from any source distribution.
;;
;; Usage: (require 'translucent-move)
;;        Ensure that a compositor such as xcompmgr is running.

(define prev-alpha-value #xffffffff)
(define temp-alpha-value #x7fffffff)

(define (lighten-alpha w)
        (setq prev-alpha-value (caddr
	    (get-x-property (window-frame-id w) '_NET_WM_WINDOW_OPACITY)))
        (cond ((eq prev-alpha-value nil) (setq prev-alpha-value #xffffffff))
	    (t (setq prev-alpha-value (aref prev-alpha-value 0))))
        (setq temp-alpha-value (/ prev-alpha-value 2))
        (set-x-property (window-frame-id w) '_NET_WM_WINDOW_OPACITY
	    (make-vector 1 temp-alpha-value) 'CARDINAL 32))

(define (restore-alpha w)
        (set-x-property (window-frame-id w) '_NET_WM_WINDOW_OPACITY
	    (make-vector 1 prev-alpha-value) 'CARDINAL 32))

(add-hook 'before-move-hook lighten-alpha t)
(add-hook 'after-move-hook restore-alpha t)

(provide 'translucent-move)
